# Simple, generic moving map for modern civil airliners
# By Ryan M
#######################################################

# This script provides the necessary properties for creating moving maps

var moving_map =
 {
 get_heading_error: func(reference, heading)
  {
  if (typeof(reference) != "scalar" or typeof(heading) != "scalar")
   {
   return 0;
   }
  var error = reference - heading;
  while (error < -180)
   {
   error += 180;
   }
  while (error > 180)
   {
   error -= 180;
   }
  return error;
  },
 get_model_distance: func(node)
  {
  var pos = node.getChild("position");
  if (pos != nil)
   {
   var x = pos.getChild("global-x");
   var y = pos.getChild("global-y");
   var z = pos.getChild("global-z");
   if (x != nil and y != nil and z != nil)
    {
    var coords = geo.Coord.new().set_xyz(x.getValue(), y.getValue(), z.getValue());
    var distance = nil;
    call(func distance = geo.aircraft_position().distance_to(coords), nil, var err = []);
    if (size(err) or distance == nil)
     {
     return 0;
     }
    else
     {
     return distance;
     }
    }
   }
  else
   {
   return 0;
   }
  },
 get_model_bearing: func(node)
  {
  var pos = node.getChild("position");
  if (pos != nil)
   {
   var x = pos.getChild("global-x");
   var y = pos.getChild("global-y");
   var z = pos.getChild("global-z");
   if (x != nil and y != nil and z != nil)
    {
    var coords = geo.Coord.new().set_xyz(x.getValue(), y.getValue(), z.getValue());
    return geo.aircraft_position().course_to(coords);
    }
   else
    {
    return 0;
    }
   }
  else
   {
   return 0;
   }
  },
 init: func
  {
  print("Moving map ... initialized");
  moving_map.update();
  },
 update: func
  {
  moving_map.width = getprop("instrumentation/moving-map/width-m");
  moving_map.height = getprop("instrumentation/moving-map/height-m");
  moving_map.range = getprop("instrumentation/moving-map/controls/range-nm");

  var aircraft_heading = props.globals.getNode("orientation/heading-magnetic-deg");
  if (aircraft_heading == nil)
   {
   return;
   }
  aircraft_heading = aircraft_heading.getValue();

  var navs = props.globals.getNode("instrumentation").getChildren("nav");
  for (var i = 0; i < size(navs); i += 1)
   {
   var node = props.globals.getNode("instrumentation/moving-map/nav[" ~ i ~ "]");
   if (node == nil)
    {
    var posx = props.globals.initNode("instrumentation/moving-map/nav[" ~ i ~ "]/pos-x-m", 0, "DOUBLE");
    var posy = props.globals.initNode("instrumentation/moving-map/nav[" ~ i ~ "]/pos-y-m", 0, "DOUBLE");
    var visible = props.globals.initNode("instrumentation/moving-map/nav[" ~ i ~ "]/visible", 0, "BOOL");
    }
   else
    {
    var posx = node.getChild("pos-x-m");
    var posy = node.getChild("pos-y-m");
    var visible = node.getChild("visible");
    }

   var in_range = navs[i].getChild("in-range");
   var distance = call(func navs[i].getChild("nav-distance").getValue() * 0.0005399568, nil, var err = []);
   var bearing = call(func navs[i].getChild("heading-deg").getValue(), nil, var err = []);
   if (in_range != nil and in_range.getBoolValue() and typeof(distance) == "scalar" and typeof(bearing) == "scalar")
    {
    var bearing_error = moving_map.get_heading_error(aircraft_heading, bearing);
    if (bearing_error < -90 or bearing_error > 90)
     {
     visible.setBoolValue(0);
     }
    else
     {
     var x = math.sin(bearing_error * D2R) * distance / moving_map.range * moving_map.width / 2;
     var y = math.cos(bearing_error * D2R) * distance / moving_map.range * moving_map.height;
     if (x > moving_map.width / 2 or x < -moving_map.width / 2 or y > moving_map.height)
      {
      visible.setBoolValue(0);
      }
     else
      {
      posx.setValue(x);
      posy.setValue(y);
      visible.setBoolValue(1);
      }
     }
    }
   else
    {
    visible.setBoolValue(0);
    }
   }

  var aircrafts = props.globals.getNode("ai/models").getChildren("aircraft");
  for (var i = 0; i < size(aircrafts); i += 1)
   {
   var node = props.globals.getNode("instrumentation/moving-map/ai[" ~ i ~ "]");
   if (node == nil)
    {
    var posx = props.globals.initNode("instrumentation/moving-map/ai[" ~ i ~ "]/pos-x-m", 0, "DOUBLE");
    var posy = props.globals.initNode("instrumentation/moving-map/ai[" ~ i ~ "]/pos-y-m", 0, "DOUBLE");
    var visible = props.globals.initNode("instrumentation/moving-map/ai[" ~ i ~ "]/visible", 0, "BOOL");
    }
   else
    {
    var posx = node.getChild("pos-x-m");
    var posy = node.getChild("pos-y-m");
    var visible = node.getChild("visible");
    }

   var valid = aircrafts[i].getChild("valid");
   var distance = moving_map.get_model_distance(aircrafts[i]);
   var bearing = moving_map.get_model_bearing(aircrafts[i]);
   if (valid != nil and valid.getBoolValue() and typeof(distance) == "scalar" and typeof(bearing) == "scalar")
    {
    if (distance == nil or bearing == nil)
     {
     visible.setBoolValue(0);
     continue;
     }
    distance *= 0.0005399568;
    var bearing_error = moving_map.get_heading_error(aircraft_heading, bearing);
    if (bearing_error < -90 or bearing_error > 90)
     {
     visible.setBoolValue(0);
     }
    else
     {
     var x = math.sin(bearing_error * D2R) * distance / moving_map.range * moving_map.width / 2;
     var y = math.cos(bearing_error * D2R) * distance / moving_map.range * moving_map.height;
     if (x > moving_map.width / 2 or x < -moving_map.width / 2 or y > moving_map.height)
      {
      visible.setBoolValue(0);
      }
     else
      {
      posx.setValue(x);
      posy.setValue(y);
      visible.setBoolValue(1);
      }
     }
    }
   else
    {
    visible.setBoolValue(0);
    }
   }

  var multiplayers = props.globals.getNode("ai/models").getChildren("multiplayer");
  for (var i = 0; i < size(multiplayers); i += 1)
   {
   var node = props.globals.getNode("instrumentation/moving-map/multiplay[" ~ i ~ "]");
   if (node == nil)
    {
    var posx = props.globals.initNode("instrumentation/moving-map/multiplay[" ~ i ~ "]/pos-x-m", 0, "DOUBLE");
    var posy = props.globals.initNode("instrumentation/moving-map/multiplay[" ~ i ~ "]/pos-y-m", 0, "DOUBLE");
    var visible = props.globals.initNode("instrumentation/moving-map/multiplay[" ~ i ~ "]/visible", 0, "BOOL");
    }
   else
    {
    var posx = node.getChild("pos-x-m");
    var posy = node.getChild("pos-y-m");
    var visible = node.getChild("visible");
    }

   var valid = multiplayers[i].getChild("valid");
   var distance = moving_map.get_model_distance(multiplayers[i]);
   var bearing = moving_map.get_model_bearing(multiplayers[i]);
   if (valid != nil and valid.getBoolValue() and typeof(distance) == "scalar" and typeof(bearing) == "scalar")
    {
    distance *= 0.0005399568;
    var bearing_error = moving_map.get_heading_error(aircraft_heading, bearing);
    if (bearing_error < -90 or bearing_error > 90)
     {
     visible.setBoolValue(0);
     }
    else
     {
     var x = math.sin(bearing_error * D2R) * distance / moving_map.range * moving_map.width / 2;
     var y = math.cos(bearing_error * D2R) * distance / moving_map.range * moving_map.height;
     if (x > moving_map.width / 2 or x < -moving_map.width / 2 or y > moving_map.height)
      {
      visible.setBoolValue(0);
      }
     else
      {
      posx.setValue(x);
      posy.setValue(y);
      visible.setBoolValue(1);
      }
     }
    }
   else
    {
    visible.setBoolValue(0);
    }
   }

  var waypoints = props.globals.getNode("autopilot/route-manager").getChildren("wp");
  for (var i = 0; i < size(waypoints); i += 1)
   {
   var node = props.globals.getNode("instrumentation/moving-map/waypoint[" ~ i ~ "]");
   if (node == nil)
    {
    var posx = props.globals.initNode("instrumentation/moving-map/waypoint[" ~ i ~ "]/pos-x-m", 0, "DOUBLE");
    var posy = props.globals.initNode("instrumentation/moving-map/waypoint[" ~ i ~ "]/pos-y-m", 0, "DOUBLE");
    var visible = props.globals.initNode("instrumentation/moving-map/waypoint[" ~ i ~ "]/visible", 0, "BOOL");
    }
   else
    {
    var posx = node.getChild("pos-x-m");
    var posy = node.getChild("pos-y-m");
    var visible = node.getChild("visible");
    }

   var distance = waypoints[i].getChild("dist");
   var bearing = waypoints[i].getChild("bearing-deg");
   if (typeof(distance) == "scalar" and typeof(bearing) == "scalar")
    {
    var bearing_error = moving_map.get_heading_error(aircraft_heading, bearing);
    if (bearing_error < -90 or bearing_error > 90)
     {
     visible.setBoolValue(0);
     }
    else
     {
     var x = math.sin(bearing_error * D2R) * distance / moving_map.range * moving_map.width / 2;
     var y = math.cos(bearing_error * D2R) * distance / moving_map.range * moving_map.height;
     if (x > moving_map.width / 2 or x < -moving_map.width / 2 or y > moving_map.height)
      {
      visible.setBoolValue(0);
      }
     else
      {
      posx.setValue(x);
      posy.setValue(y);
      visible.setBoolValue(1);
      }
     }
    }
   else
    {
    visible.setBoolValue(0);
    }
   }

  if (props.globals.getNode("instrumentation/moving-map/controls/TERR").getBoolValue())
   {
   if (props.globals.getNode("instrumentation/moving-map/terrain-elevation/use-high-resolution").getBoolValue())
    {
    moving_map.update_terrain_hi();
    }
   else
    {
    moving_map.update_terrain();
    }
   settimer(moving_map.update, 2);
   }
  else
   {
   settimer(moving_map.update, 1);
   }
  },
 update_terrain: func
  {
  var pos = geo.aircraft_position();
  var hdg = getprop("orientation/heading-magnetic-deg");
  var path = "instrumentation/moving-map/terrain-elevation/";

  pos.apply_course_distance(hdg - 90, moving_map.range / 2 * 1852);
  for (var i = 0; i < 16; i += 1)
   {
   for (var j = 0; j < 16; j += 1)
    {
    var info = geodinfo(pos.lat(), pos.lon());
    if (info != nil and info[1] != nil)
     {
     if (info[1].solid)
      {
      setprop(path ~ "row[" ~ i ~ "]/col[" ~ j ~ "]", info[0] * M2FT);
      }
     else
      {
      setprop(path ~ "row[" ~ i ~ "]/col[" ~ j ~ "]", 0);
      }
     }
    else
     {
     setprop(path ~ "row[" ~ i ~ "]/col[" ~ j ~ "]", 0);
     }

    pos.apply_course_distance(hdg + 90, moving_map.range / 16 * 1852);
    }
   pos.apply_course_distance(hdg - 90, moving_map.range * 1852);
   pos.apply_course_distance(hdg, moving_map.range / 16 * 1852);
   }
  },
 update_terrain_hi: func
  {
  var pos = geo.aircraft_position();
  var hdg = getprop("orientation/heading-magnetic-deg");
  var path = "instrumentation/moving-map/terrain-elevation/";

  pos.apply_course_distance(hdg - 90, moving_map.range / 2 * 1852);
  for (var i = 0; i < 32; i += 1)
   {
   for (var j = 0; j < 32; j += 1)
    {
    var info = geodinfo(pos.lat(), pos.lon());
    if (info != nil and info[1] != nil)
     {
     if (info[1].solid)
      {
      setprop(path ~ "row[" ~ i ~ "]/col[" ~ j ~ "]", info[0] * M2FT);
      }
     else
      {
      setprop(path ~ "row[" ~ i ~ "]/col[" ~ j ~ "]", 0);
      }
     }
    else
     {
     setprop(path ~ "row[" ~ i ~ "]/col[" ~ j ~ "]", 0);
     }

    pos.apply_course_distance(hdg + 90, moving_map.range / 32 * 1852);
    }
   pos.apply_course_distance(hdg - 90, moving_map.range * 1852);
   pos.apply_course_distance(hdg, moving_map.range / 32 * 1852);
   }
  }
 };
setlistener("sim/signals/fdm-initialized", func
 {
 settimer(moving_map.init, 2);
 }, 0, 0);
