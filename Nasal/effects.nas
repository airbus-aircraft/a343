# Airbus A340-300
# Nasal effects
#################

## Livery select
aircraft.livery.init("Aircraft/A343/Models/Liveries");

setlistener("sim/model/livery/texture", func(base)
 {
 setprop("sim/model/livery/texture-path[0]", "../Models/" ~ base.getValue());
 setprop("sim/model/livery/texture-path[1]", "../../Models/" ~ base.getValue());
 }, 1, 1);

## Tire smoke/rain
var tiresmoke_system = aircraft.tyresmoke_system.new(0, 2, 4, 5);
aircraft.rain.init();
