# Airbus A340-300
# Nasal landing gear functions
#################

# 1 ft/m
var FT2M = 0.3048;

var gears =
 {
 update: func
  {
  gears.tilt(props.globals.getNode("gear/gear[1]/compression-ft"), props.globals.getNode("gear/gear[2]/compression-ft"), props.globals.getNode("gear/gear-left-tilt-deg", 1));
  gears.tilt(props.globals.getNode("gear/gear[3]/compression-ft"), props.globals.getNode("gear/gear[4]/compression-ft"), props.globals.getNode("gear/gear-right-tilt-deg", 1));
  },
 tilt: func(fwd, aft, out)
  {
  var Gf = fwd.getValue();
  var Ga = aft.getValue();

  # old formula, doesn't seem to work well anymore
  # tan ((Gf - Ga) * FT2M / 0.99) * R2D
  #out.setValue(math.tan((Gf - Ga) * FT2M / 0.99) * R2D);

  # asin ((Ga - Gf - 0.0719) * FT2M / 0.99) * R2D - 29.104
  out.setValue(math.asin((Ga - Gf - 0.0719) * FT2M / 0.99) * R2D - 29.104);
  }
 };
