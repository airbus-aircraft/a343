# Airbus A340-300
# Engine control system
#################

# NOTE: Update functions are called in systems.nas

var engine =
 {
 new: func(no)
  {
  var m =
   {
   parents: [engine]
   };
  m.number = no;

  m.on_fire = props.globals.getNode("engines/engine[" ~ no ~ "]/on-fire", 1);
  m.on_fire.setBoolValue(0);
  m.reverser = props.globals.getNode("controls/engines/engine[" ~ no ~ "]/reverser", 1);
  m.reverser.setBoolValue(0);
  m.reverser_angle_rad = props.globals.getNode("fdm/jsbsim/propulsion/engine[" ~ no ~ "]/reverser-angle-rad", 1);
  m.reverser_angle_rad.setValue(0);
  m.reverser_pos_norm = props.globals.getNode("engines/engine[" ~ no ~ "]/reverser-pos-norm", 1);
  m.reverser_pos_norm.setValue(0);
  m.serviceable = props.globals.getNode("sim/failure-manager/engines/engine[" ~ no ~ "]/serviceable", 1);
  m.serviceable.setBoolValue(1);
  m.throttle = props.globals.getNode("controls/engines/engine[" ~ no ~ "]/throttle", 1);
  m.throttle.setValue(0);

  return m;
  },
 update: func
  {
  if (me.on_fire.getBoolValue())
   {
   me.serviceable.setBoolValue(0);
   }
  },
 reverse_thrust: func
  {
  if (me.throttle.getValue() == 0 and props.globals.getNode("gear/gear[1]/wow").getBoolValue())
   {
   if (me.reverser_pos_norm.getValue() == 0)
    {
    interpolate(me.reverser_pos_norm.getPath(),
     1, 1.4
    );
    me.reverser_angle_rad.setValue(math.pi);
    me.reverser.setBoolValue(1);
    }
   elsif (me.reverser_pos_norm.getValue() == 1)
    {
    interpolate(me.reverser_pos_norm.getPath(),
     0, 1.4
    );
    me.reverser_angle_rad.setValue(0);
    me.reverser.setBoolValue(0);
    }
   }
  }
 };
var engine1 = engine.new(0);
var engine2 = engine.new(1);
var engine3 = engine.new(2);
var engine4 = engine.new(3);

# startup/shutdown functions
var startup = func
 {
 setprop("controls/engines/engine[0]/cutoff", 1);
 setprop("controls/engines/engine[1]/cutoff", 1);
 setprop("controls/engines/engine[2]/cutoff", 1);
 setprop("controls/engines/engine[3]/cutoff", 1);
 setprop("controls/engines/engine[0]/starter", 1);
 setprop("controls/engines/engine[1]/starter", 1);
 setprop("controls/engines/engine[2]/starter", 1);
 setprop("controls/engines/engine[3]/starter", 1);
 setprop("controls/electric/avionics-switch", 1);
 setprop("controls/electric/battery-switch", 1);
 setprop("controls/lighting/instruments-norm", 0.8);

 settimer(func
  {
  setprop("controls/engines/engine[0]/cutoff", 0);
  setprop("controls/engines/engine[1]/cutoff", 0);
  setprop("controls/engines/engine[2]/cutoff", 0);
  setprop("controls/engines/engine[3]/cutoff", 0);
  }, 2);
 };
var shutdown = func
 {
 setprop("controls/engines/engine[0]/cutoff", 1);
 setprop("controls/engines/engine[1]/cutoff", 1);
 setprop("controls/engines/engine[2]/cutoff", 1);
 setprop("controls/engines/engine[3]/cutoff", 1);
 setprop("controls/electric/avionics-switch", 0);
 setprop("controls/electric/battery-switch", 0);
 setprop("controls/lighting/instruments-norm", 0);
 };

# listener to activate these functions accordingly
setlistener("sim/model/start-idling", func(idle)
 {
 var run = idle.getBoolValue();
 if (run)
  {
  startup();
  }
 else
  {
  shutdown();
  }
 }, 0, 0);
