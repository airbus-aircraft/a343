# Airbus A340-300
# Fly-by-wire control system
# Master file for FBW functions and computers
#############################################

var prim0 = prim.new(0);
var prim1 = prim.new(1);
var prim2 = prim.new(2);
var sec0 = sec.new(0);
var sec1 = sec.new(1);

var ACTUATOR_UPDATE_PERIOD = 0;
var actuator =
 {
 elevator: props.globals.initNode("fcs/actuator-controls/elevator", 0, "DOUBLE"),
 left_outboard_aileron: props.globals.initNode("fcs/actuator-controls/left-outboard-aileron", 0, "DOUBLE"),
 left_inboard_aileron: props.globals.initNode("fcs/actuator-controls/left-inboard-aileron", 0, "DOUBLE"),
 right_outboard_aileron: props.globals.initNode("fcs/actuator-controls/right-outboard-aileron", 0, "DOUBLE"),
 right_inboard_aileron: props.globals.initNode("fcs/actuator-controls/right-inboard-aileron", 0, "DOUBLE"),
 rudder: props.globals.initNode("fcs/actuator-controls/rudder", 0, "DOUBLE")
 };

var update_actuator_controls = func
 {
 var updated = 0;
 var prims = props.globals.getNode("fcs").getChildren("prim");
 for (var i = 0; i < size(prims); i += 1)
  {
  var status = prims[i].getChild("status");
  if (status != nil and status.getBoolValue())
   {
   actuator.elevator.setValue(prims[i].getChild("elevator").getValue());
   actuator.left_outboard_aileron.setValue(prims[i].getChild("left-outboard-aileron").getValue());
   actuator.left_inboard_aileron.setValue(prims[i].getChild("left-inboard-aileron").getValue());
   actuator.right_outboard_aileron.setValue(prims[i].getChild("right-outboard-aileron").getValue());
   actuator.right_inboard_aileron.setValue(prims[i].getChild("right-inboard-aileron").getValue());
   actuator.rudder.setValue(prims[i].getChild("yaw-damper").getValue());
   updated = 1;
   break;
   }
  }
 if (!updated)
  {
  # Hand control over to the secondary flight computeres
  }

 settimer(update_actuator_controls, ACTUATOR_UPDATE_PERIOD);
 };
settimer(update_actuator_controls, ACTUATOR_UPDATE_PERIOD);

print("A343 Fly-by-wire ... initialized");
