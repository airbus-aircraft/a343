# Airbus A340-300
# Fly-by-wire control system
# SEC computer
############################

var sec =
 {
 new: func(no)
  {
  var m =
   {
   parents: [sec]
   };
  m.number = no;
  return m;
  },
 lateral:
  {
  # Direct Law for lateral control
  layer1: func(comp)
   {
   },
  # Alternate Law for lateral control
  layer2: func(comp)
   {
   },
  # Normal Law for lateral control
  layer3: func(comp)
   {
   }
  },
 update: func
  {
  }
 };
