# Airbus A340-300
# Fly-by-wire control system
# PRIM computer
############################

var PRIM_UPDATE_PERIOD = 0;

var prim =
 {
 new: func(no)
  {
  var m =
   {
   parents: [prim]
   };
  m.number = no;

  m.status = props.globals.initNode("fcs/prim[" ~ no ~ "]/status", 0, "BOOL");
  m.serviceable = props.globals.initNode("fcs/prim[" ~ no  ~ "]/serviceable", 1, "BOOL");

  m.elevator = props.globals.initNode("fcs/prim[" ~ no ~ "]/elevator", 0, "DOUBLE");
  m.left_outboard_aileron = props.globals.initNode("fcs/prim[" ~ no ~ "]/left-outboard-aileron", 0, "DOUBLE");
  m.left_inboard_aileron = props.globals.initNode("fcs/prim[" ~ no ~ "]/left-inboard-aileron", 0, "DOUBLE");
  m.right_outboard_aileron = props.globals.initNode("fcs/prim[" ~ no ~ "]/right-outboard-aileron", 0, "DOUBLE");
  m.right_inboard_aileron = props.globals.initNode("fcs/prim[" ~ no ~ "]/right-inboard-aileron", 0, "DOUBLE");
  m.yaw_damper = props.globals.initNode("fcs/prim[" ~ no ~ "]/yaw-damper", 0, "DOUBLE");
  m.spoiler1 = props.globals.initNode("fcs/prim[" ~ no ~ "]/spoiler1", 0, "DOUBLE");
  m.spoiler2 = props.globals.initNode("fcs/prim[" ~ no ~ "]/spoiler2", 0, "DOUBLE");
  m.spoiler4 = props.globals.initNode("fcs/prim[" ~ no ~ "]/spoiler4", 0, "DOUBLE");
  m.spoiler5 = props.globals.initNode("fcs/prim[" ~ no ~ "]/spoiler5", 0, "DOUBLE");

  settimer(func
   {
   m.update();
   }, PRIM_UPDATE_PERIOD);

  return m;
  },
 # Computation functions
 # Each function returns a status: 1 for success or 0 for failure
 # Direct Law for lateral control
 lateral_layer1: func
  {
  if (!me.serviceable.getBoolValue())
   {
   return 0;
   }

  var aileron = getprop("controls/flight/aileron");
  me.left_outboard_aileron.setValue(aileron * -1);
  me.left_inboard_aileron.setValue(aileron * -1);
  me.right_outboard_aileron.setValue(aileron);
  me.right_inboard_aileron.setValue(aileron);

  var rudder = getprop("controls/flight/rudder");
  me.yaw_damper.setValue(rudder);

  return 1;
  },
 # Alternate Law for lateral control
 lateral_layer2: func
  {
  return 1;
  },
 # Normal Law for lateral control
 lateral_layer3: func
  {
  return 1;
  },
 # Direct Law for vertical control
 vertical_layer1: func
  {
  if (!me.serviceable.getBoolValue())
   {
   return 0;
   }

  var elevator = getprop("controls/flight/elevator");
  me.elevator.setValue(elevator);

  return 1;
  },
 # Alternate Law for vertical control
 vertical_layer2: func(comp)
  {
  return 1;
  },
  # Normal Law for vertical control
 vertical_layer3: func(comp)
  {
  return 1;
  },
 update: func
  {
  # Execute layer 1
  me.status.setBoolValue(call(func me.lateral_layer1(), nil, var err = []) and call(func me.vertical_layer1(), nil, var err = []));
  # Execute layer 2
  call(func me.lateral_layer2(), nil, var err = []);
  call(func me.vertical_layer2(), nil, var err = []);
  # Execute layer 3
  call(func me.lateral_layer3(), nil, var err = []);
  call(func me.vertical_layer2(), nil, var err = []);

  # Next iteration
  settimer(func me.update(), PRIM_UPDATE_PERIOD);
  }
 };
