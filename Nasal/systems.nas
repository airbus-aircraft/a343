# Airbus A330-300
# Aircraft systems
#################

# NOTE: This file contains a loop for running all update functions, so it should be loaded last

# Main systems update loop
var systems =
 {
 stopUpdate: 0,
 init: func
  {
  print("A343 aircraft systems ... initialized");
  systems.stop();
  settimer(func
   {
   systems.stopUpdate = 0;
   systems.update();
   }, 0.5);
  setprop("sim/model/start-idling", 0);
  },
 stop: func
  {
  systems.stopUpdate = 1;
  },
 update: func
  {
  engine1.update();
  engine2.update();
  engine3.update();
  engine4.update();
  update_electrical();
  instruments.update();
  gears.update();

  # stop calling our systems code if the stop() function was called or the aircraft crashes
  if (!systems.stopUpdate and !props.globals.getNode("sim/crashed").getBoolValue())
   {
   systems.timer = settimer(systems.update, 0);
   }
  }
 };

# call init() 2 seconds after the FDM is ready
setlistener("sim/signals/fdm-initialized", func
 {
 settimer(systems.init, 2);
 }, 0, 0);
# call init() if the simulator resets
setlistener("sim/signals/reinit", func(reinit)
 {
 if (reinit.getBoolValue())
  {
  systems.init();
  }
 }, 0, 0);

## Prevent landing gear retraction if any of the gears are compressed
setlistener("controls/gear/gear-down", func(gear_down)
 {
 var down = gear_down.getBoolValue();
 if (!down and (props.globals.getNode("gear/gear[0]/wow").getBoolValue() or props.globals.getNode("gear/gear[2]/wow").getBoolValue() or props.globals.getNode("gear/gear[4]/wow").getBoolValue()))
  {
  gear_down.setBoolValue(1);
  }
 }, 0, 0);

## Instrumentation
var instruments =
 {
 update: func
  {
#  instruments.set_hsi_bugs();
#  instruments.set_spd_bugs();
  instruments.update_engines();
  },
 set_spd_bugs: func
  {
  setprop("instrumentation/pfd/ias-dif-kt", getprop("autopilot/settings/target-speed-kt") - getprop("velocities/airspeed-kt"));
  setprop("instrumentation/pfd/mach-dif-kt", (getprop("autopilot/settings/target-speed-mach") - getprop("velocities/mach")) * 600);
  },
 update_engines: func
  {
  var engines = props.globals.getNode("engines").getChildren("engine");
  for (var i = 0; i < size(engines); i += 1)
   {
   var egt_degf = engines[i].getChild("egt-degf", 1);
   var egt_degc = engines[i].getChild("egt-degc", 1);
   if (egt_degf != nil)
    {
    egt_degc.setValue((egt_degf.getValue() - 32) * 1.8);
    }
   }
  }
 };
